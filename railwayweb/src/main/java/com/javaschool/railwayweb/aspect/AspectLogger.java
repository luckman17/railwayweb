package com.javaschool.railwayweb.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AspectLogger {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Before("execution(* com.javaschool.railwayweb.controllers..*.*(..))"
			+ " || execution(* com.javaschool.railwayweb.dao..*.*(..))"
			+ " || execution(* com.javaschool.railwayweb.model..*.*(..))"
			+ " || execution(* com.javaschool.railwayweb.service..*.*(..))")
    public void logBefore(JoinPoint point) {
		Object[] args = point.getArgs();
		StringBuilder loggerStr = new StringBuilder();
		loggerStr.append(point.getSignature().getDeclaringTypeName() + "." + point.getSignature().getName() + " called with args(");
		for(int i = 0; i < args.length; i++) {
			if (i > 0) {
				loggerStr.append(", ");
			}
			loggerStr.append(args[i]);
		}
		loggerStr.append(")");
		logger.info(loggerStr);
    }

	@AfterReturning(pointcut="execution(* com.javaschool.railwayweb.controllers..*.*(..))"
			+ " || execution(* com.javaschool.railwayweb.dao..*.*(..))"
			+ " || execution(* com.javaschool.railwayweb.model..*.*(..))"
			+ " || execution(* com.javaschool.railwayweb.service..*.*(..))",
			returning="retVal")
    public void logAfter(JoinPoint point, Object retVal) {
		StringBuilder loggerStr = new StringBuilder();
		loggerStr.append(point.getSignature().getDeclaringTypeName() + "." + point.getSignature().getName() + " returned ");
		loggerStr.append(retVal);
		logger.info(loggerStr);
    }

	
}
