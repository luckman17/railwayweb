package com.javaschool.railwayweb.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.javaschool.railwayweb.dao.RouteDao;
import com.javaschool.railwayweb.dao.ScheduleDao;
import com.javaschool.railwayweb.model.Schedule;
import com.javaschool.railwayweb.service.ScheduleService;

@RequestMapping("/schedule/add")
@Controller
public class AddScheduleController {
	@Autowired
	RouteDao routeDao;
	@Autowired
	ScheduleService scheduleService;
	
	@RequestMapping(method=RequestMethod.GET)
	public String showPage(Model model, @ModelAttribute("errorMsg") String errorMsg) {
		model.addAttribute("schedule", new Schedule());
		model.addAttribute("routeList", routeDao.getAll());
		model.addAttribute("msg", errorMsg);
		return "schedule/addSchedule";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String addSchedule(Integer route,
				@DateTimeFormat(pattern="dd.MM.yyyy") Date dateStart,
				Integer seats, Model model, RedirectAttributes redirectAttributes) {
		String result = scheduleService.addSchedule(routeDao.find(route), dateStart, seats);
		if (result.equals(""))
			return "redirect:/";
		else {
			redirectAttributes.addAttribute("errorMsg", result);
			return "redirect:/schedule/add";
		}
	}
}
