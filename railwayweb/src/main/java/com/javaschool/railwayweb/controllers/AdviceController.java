package com.javaschool.railwayweb.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class AdviceController {

	@ExceptionHandler(Exception.class)
	public ModelAndView defaultHandler(Exception exception) {
		ModelAndView model = new ModelAndView();
		model.addObject("exception", exception);
		model.setViewName("errorPage");
		return model;
	}
}
