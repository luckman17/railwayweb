package com.javaschool.railwayweb.controllers;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityTransaction;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.javaschool.railwayweb.dao.Managers;
import com.javaschool.railwayweb.dao.ScheduleDao;
import com.javaschool.railwayweb.dao.TicketDao;
import com.javaschool.railwayweb.dto.BuyTicketDto;
import com.javaschool.railwayweb.dto.PassengerTicketDto;
import com.javaschool.railwayweb.model.Passenger;
import com.javaschool.railwayweb.model.RouteLines;
import com.javaschool.railwayweb.model.Schedule;
import com.javaschool.railwayweb.model.Station;
import com.javaschool.railwayweb.service.RouteService;
import com.javaschool.railwayweb.service.ScheduleService;
import com.javaschool.railwayweb.service.TicketService;

@Controller
@RequestMapping("/buyTicket")
public class BuyTicketController {
	@Autowired
	ScheduleDao scheduleDao;

	@Autowired
	TicketService ticketService;
	@Autowired
	RouteService routeService;
/*	
	@RequestMapping(method = RequestMethod.GET)
	public String showPassengerInfoForm(Model model, @RequestParam("id") Integer id) {
		model.addAttribute("buyTicketDto", new BuyTicketDto());
		model.addAttribute("msg", "");
		return "ticket/buyTicket";
	}
*/	
	@RequestMapping(value="/form", method=RequestMethod.GET)
	@Transactional
	public String showTicketInfoPage(Model model, @RequestParam("id") Long id) {
		Schedule schedule = scheduleDao.find(id);
		schedule.getTickets().size();
		BuyTicketDto buyTicketDto = new BuyTicketDto();
		buyTicketDto.setScheduleId(id);

		Map<Integer, Station> stationMap = routeService.getStationMap(schedule.getRoute());
		
		model.addAttribute("stationMap", stationMap);
		model.addAttribute("buyTicketDto", buyTicketDto);
		if (!model.containsAttribute("msg"))
			model.addAttribute("msg", "");
		return "ticket/buyTicketStation";
	}

	@RequestMapping(value="/form", method=RequestMethod.POST)
	@Transactional
	public String showPassengerInfoPage(Model model, BuyTicketDto buyTicketDto) {
		Schedule schedule = scheduleDao.find(buyTicketDto.getScheduleId());
		String check = ticketService.checkPoints(schedule, buyTicketDto.getPointFrom(), buyTicketDto.getPointTo());
		if (check != "") {
			model.addAttribute("msg", check);
			return this.showTicketInfoPage(model, buyTicketDto.getScheduleId());
		}
		model.addAttribute("buyTicketDto", buyTicketDto);
		model.addAttribute("msg", "");		
		return "ticket/buyTicketPassenger";
	}
	
	@RequestMapping(value="/result", method = RequestMethod.POST)
	@Transactional
	public String successBuyTicket(@Valid BuyTicketDto buyTicketDto,
			BindingResult br, Model model) {
		if (br.hasErrors()) {
			model.addAttribute("buyTicketDto", buyTicketDto);
			model.addAttribute("msg", "");
			return "ticket/buyTicketPassenger";
		}
		Passenger p = buyTicketDto.getPassenger();
		Schedule s = scheduleDao.find(buyTicketDto.getScheduleId());
		String res = ticketService.buyTicket(p, s, buyTicketDto.getPointFrom(), buyTicketDto.getPointTo());		
		
		model.addAttribute("msg", res);
		PassengerTicketDto passengerTicketDto = new PassengerTicketDto();
		if (res.equals("")) {
			passengerTicketDto.setFirstname(p.getFirstname());
			passengerTicketDto.setLastname(p.getLastname());
			passengerTicketDto.setStationFrom(routeService.getStation(s.getRoute(), buyTicketDto.getPointFrom()).getName());
			passengerTicketDto.setStationTo(routeService.getStation(s.getRoute(), buyTicketDto.getPointTo()).getName());
		}
		
		model.addAttribute("passengerTicket", passengerTicketDto);
		
		return "ticket/buySuccess";
	}

}
