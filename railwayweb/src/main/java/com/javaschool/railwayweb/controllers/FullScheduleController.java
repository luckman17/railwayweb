package com.javaschool.railwayweb.controllers;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.javaschool.railwayweb.dao.ScheduleDao;
import com.javaschool.railwayweb.dao.StationDao;
import com.javaschool.railwayweb.dto.ScheduleRouteLineDto;
import com.javaschool.railwayweb.model.Schedule;
import com.javaschool.railwayweb.model.ScheduleFilter;
import com.javaschool.railwayweb.model.Station;
import com.javaschool.railwayweb.service.ScheduleService;

@Controller
@RequestMapping({"/"})
public class FullScheduleController {
	
	@Autowired
	ScheduleDao scheduleDao;
	@Autowired
	StationDao stationDao;
	@Autowired
	ScheduleService scheduleService;
	
	@RequestMapping
	public String showSchedulePage(ScheduleFilter scheduleFilter, Model model) {
		List<Schedule> scheduleList;
		if (scheduleFilter == null) {
			scheduleList = scheduleDao.getAll();
		}
		else {			
			scheduleList = scheduleDao.getFilteredSchedule(scheduleFilter);
		}
		model.addAttribute("scheduleList", scheduleList);
		model.addAttribute("applyFilter", scheduleFilter == null ? new ScheduleFilter() : scheduleFilter);
		List<Station> stationList = stationDao.getAll();
		Map<Long, String> stationMap = new LinkedHashMap<Long, String>();
		for (Station s : stationList) {
			stationMap.put(s.getId(), s.getName());
		}
		model.addAttribute("stationMap", stationMap);
		
		return "schedule/index";
	}
	
	@RequestMapping("/schedule/routeLines")
	public String showLines(Model model, @RequestParam("id") long id) {
		List<ScheduleRouteLineDto> srlDtoList = scheduleService.getScheduleRouteLines(id);
		model.addAttribute("scheduleRouteLines", srlDtoList);
		model.addAttribute("msg", "");
		Schedule schedule = scheduleDao.find(id);
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		model.addAttribute("infoMsg", "Route: " + schedule.getRoute().getRouteName()
				+ ", departure date: " + sdf.format(schedule.getDateStart()));
		return "schedule/scheduleRouteLines";
	}
}
