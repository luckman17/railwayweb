package com.javaschool.railwayweb.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.javaschool.railwayweb.dto.PassengerTicketDto;
import com.javaschool.railwayweb.service.PassengerListService;

@Controller
@RequestMapping("/passengerList")
public class PassengerListController {
	@Autowired
	PassengerListService passengerListService;
	
	@RequestMapping
	@Transactional
	public String showPassengerList(Model model, @RequestParam("id") Integer id) {
		List<PassengerTicketDto> passengerList = passengerListService.getPassengerList(id);
		model.addAttribute("passengerList", passengerList);
		return "ticket/passengerList";
	}
}
