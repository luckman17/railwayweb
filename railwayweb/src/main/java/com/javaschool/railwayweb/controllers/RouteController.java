package com.javaschool.railwayweb.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javaschool.railwayweb.dao.RouteDao;
import com.javaschool.railwayweb.dao.StationDao;
import com.javaschool.railwayweb.model.Route;
import com.javaschool.railwayweb.model.Station;
import com.javaschool.railwayweb.service.RouteService;

@Controller
@RequestMapping("/route")
public class RouteController {
	
	@Autowired
	RouteDao routeDao;
	@Autowired
	StationDao stationDao;
	@Autowired
	RouteService routeService;
	
	@RequestMapping
	public String showRoutePage(Model model) {
		model.addAttribute("routeList", routeDao.getAll());
		return "route/routeList";
	}
	
	@RequestMapping("/add")
	public String showAddRoutePage(Model model) {
		model.addAttribute("stationList", stationDao.getAll());
		return "route/addRoute";
	}
	
	@RequestMapping(value="/send", method = RequestMethod.POST)
	@ResponseBody
	public String addRoute(@RequestParam String routeLines,
				@RequestParam String routeName, HttpServletRequest request) {
		String result = routeService.addRoute(routeName, routeLines);
		if (!result.equals("")) {
			return result;
		}
		String url = request.getRequestURL().toString();
		url = url.substring(0, url.length() - 1);
		int idx = url.lastIndexOf("/");
		url = url.substring(0, idx);		// ...railwayweb/route
		return url;
	}
}
