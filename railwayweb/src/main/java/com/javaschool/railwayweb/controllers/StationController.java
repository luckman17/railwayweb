package com.javaschool.railwayweb.controllers;

import javax.persistence.EntityTransaction;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.javaschool.railwayweb.dao.Managers;
import com.javaschool.railwayweb.dao.StationDao;
import com.javaschool.railwayweb.model.Station;
import com.javaschool.railwayweb.service.StationService;

@Controller
@RequestMapping("/station")
public class StationController {
	
	@Autowired
	StationDao stationDao;
	@Autowired
	StationService stationService;
	
	private void addAttributes(Model model) {
		model.addAttribute("station", new Station());
		model.addAttribute("stationList", stationDao.getAll());
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String showPage(Model model, @ModelAttribute("message") String message) {
		addAttributes(model);
		model.addAttribute("msg", message);
		return "station/stationList";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	@Transactional
	public String addStation(@Valid Station s, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("station", s);
			model.addAttribute("stationList", stationDao.getAll());
			return "station/stationList";
		}
		stationDao.insert(s);
		addAttributes(model);
		return "redirect:/station";
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteStation(Model model, @RequestParam("id") long id, RedirectAttributes redirectAttributes) {
		String result = stationService.deleteStation(id);
		redirectAttributes.addAttribute("message", result);
		return "redirect:/station";
	}
}
