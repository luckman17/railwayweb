package com.javaschool.railwayweb.dao;

import java.text.SimpleDateFormat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.javaschool.railwayweb.model.*;


class City {
	static final String SPB = "Saint-Petersburg";
	static final String TVR = "Tver";
	static final String MSK = "Moscow";
	static final String RZN = "Ryazan";
	static final String SML = "Smolensk";
	static final String VLG = "Vologda";
	static final String NNG = "Nizhni Novgorod";
	static final String PRM = "Perm";
	static final String KZN = "Kazan";
}
public class CreateTestData {
	public static void main(String[] args) throws Exception {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("railwayticket");
		
		EntityManager em = emf.createEntityManager();
		
		// delete data
		EntityTransaction trans = em.getTransaction();
		try {
			trans.begin();
			em.createQuery("delete from Ticket t").executeUpdate();
			em.createQuery("delete from RouteLines s").executeUpdate();
			em.createQuery("delete from Schedule s").executeUpdate();
			em.createQuery("delete from Station s").executeUpdate();
			em.createQuery("delete from Route s").executeUpdate();
			em.createQuery("delete from Passenger p").executeUpdate();
			trans.commit();
		}
		catch (Exception e) {
			if (trans.isActive()) {
				trans.rollback();
				e.printStackTrace();
				return;
			}
		}
		
		trans.begin();
		
		// add Stations
		StationDao stationDao = new StationDao();
		stationDao.setEntityManager(em);
		Station s;
		
		s = new Station();
		s.setName(City.SPB);
		stationDao.insert(s);		
		s = new Station();
		s.setName(City.TVR);
		stationDao.insert(s);		
		s = new Station();
		s.setName(City.MSK);
		stationDao.insert(s);
		s = new Station();
		s.setName(City.RZN);
		stationDao.insert(s);
		s = new Station();
		s.setName(City.SML);
		stationDao.insert(s);
		s = new Station();
		s.setName(City.VLG);
		stationDao.insert(s);
		s = new Station();
		s.setName(City.NNG);
		stationDao.insert(s);
		s = new Station();
		s.setName(City.PRM);
		stationDao.insert(s);
		s = new Station();
		s.setName(City.KZN);
		stationDao.insert(s);		

		// add Routes
		RouteDao stDao = new RouteDao();
		stDao.setEntityManager(em);
		RouteLinesDao stlDao = new RouteLinesDao();
		stlDao.setEntityManager(em);
		RouteLines stl;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		
		Route template1 = new Route();
		template1.setRouteName("Spb - Smolensk");
		stDao.insert(template1);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template1, 1));
		stl.setStation(stationDao.findByName(City.SPB));
		stl.setTimePass(sdf.parse("20:15"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template1, 2));
		stl.setStation(stationDao.findByName(City.TVR));
		stl.setTimePass(sdf.parse("01:39"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template1, 3));
		stl.setStation(stationDao.findByName(City.MSK));
		stl.setTimePass(sdf.parse("05:00"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template1, 4));
		stl.setStation(stationDao.findByName(City.SML));
		stl.setTimePass(sdf.parse("08:52"));
		stlDao.insert(stl);
		
		Route template2 = new Route();
		template2.setRouteName("Msk - Spb nightly");
		stDao.insert(template2);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template2, 1));
		stl.setStation(stationDao.findByName(City.MSK));
		stl.setTimePass(sdf.parse("00:44"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template2, 2));
		stl.setStation(stationDao.findByName(City.TVR));
		stl.setTimePass(sdf.parse("02:34"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template2, 3));
		stl.setStation(stationDao.findByName(City.SPB));
		stl.setTimePass(sdf.parse("08:48"));
		stlDao.insert(stl);
		
		Route template3 = new Route();
		template3.setRouteName("Kazan - Spb");
		stDao.insert(template3);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template3, 1));
		stl.setStation(stationDao.findByName(City.KZN));
		stl.setTimePass(sdf.parse("14:05"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template3, 2));
		stl.setStation(stationDao.findByName(City.NNG));
		stl.setTimePass(sdf.parse("18:52"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template3, 3));
		stl.setStation(stationDao.findByName(City.VLG));
		stl.setTimePass(sdf.parse("23:30"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template3, 4));
		stl.setStation(stationDao.findByName(City.SPB));
		stl.setTimePass(sdf.parse("05:45"));
		stlDao.insert(stl);
		
		Route template4 = new Route();
		template4.setRouteName("Spb - Ryazan");
		stDao.insert(template4);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template4, 1));
		stl.setStation(stationDao.findByName(City.SPB));
		stl.setTimePass(sdf.parse("20:36"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template4, 2));
		stl.setStation(stationDao.findByName(City.TVR));
		stl.setTimePass(sdf.parse("01:59"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template4, 3));
		stl.setStation(stationDao.findByName(City.MSK));
		stl.setTimePass(sdf.parse("04:07"));
		stlDao.insert(stl);
		stl = new RouteLines();
		stl.setKey(new RouteLinesPK(template4, 4));
		stl.setStation(stationDao.findByName(City.RZN));
		stl.setTimePass(sdf.parse("07:45"));
		stlDao.insert(stl);
		
		
		// add schedule
		Schedule schedule;
		ScheduleDao scheduleDao = new ScheduleDao();
		scheduleDao.setEntityManager(em);
		SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy");
		
		schedule = new Schedule();
		schedule.setSeats(5);
		schedule.setRoute(template1);
		schedule.setDateStart(formatDate.parse("05.05.2014"));
		scheduleDao.insert(schedule);
		
		schedule = new Schedule();
		schedule.setSeats(4);
		schedule.setRoute(template1);
		schedule.setDateStart(formatDate.parse("07.05.2014"));
		scheduleDao.insert(schedule);

		schedule = new Schedule();
		schedule.setSeats(7);
		schedule.setRoute(template1);
		schedule.setDateStart(formatDate.parse("09.05.2014"));
		scheduleDao.insert(schedule);
		
		schedule = new Schedule();
		schedule.setSeats(9);
		schedule.setRoute(template1);
		schedule.setDateStart(formatDate.parse("11.05.2014"));
		scheduleDao.insert(schedule);

		schedule = new Schedule();
		schedule.setSeats(8);
		schedule.setRoute(template2);
		schedule.setDateStart(formatDate.parse("05.05.2014"));
		scheduleDao.insert(schedule);		
		
		schedule = new Schedule();
		schedule.setSeats(5);
		schedule.setRoute(template2);
		schedule.setDateStart(formatDate.parse("06.05.2014"));
		scheduleDao.insert(schedule);

		schedule = new Schedule();
		schedule.setSeats(3);
		schedule.setRoute(template2);
		schedule.setDateStart(formatDate.parse("07.05.2014"));
		scheduleDao.insert(schedule);
		
		schedule = new Schedule();
		schedule.setSeats(5);
		schedule.setRoute(template2);
		schedule.setDateStart(formatDate.parse("08.05.2014"));
		scheduleDao.insert(schedule);
		
		
		schedule = new Schedule();
		schedule.setSeats(7);
		schedule.setRoute(template3);
		schedule.setDateStart(formatDate.parse("06.05.2014"));
		scheduleDao.insert(schedule);

		schedule = new Schedule();
		schedule.setSeats(9);
		schedule.setRoute(template3);
		schedule.setDateStart(formatDate.parse("07.05.2014"));
		scheduleDao.insert(schedule);
		
		schedule = new Schedule();
		schedule.setSeats(5);
		schedule.setRoute(template4);
		schedule.setDateStart(formatDate.parse("06.05.2014"));
		scheduleDao.insert(schedule);
		
		schedule = new Schedule();
		schedule.setSeats(4);
		schedule.setRoute(template4);
		schedule.setDateStart(formatDate.parse("08.05.2014"));
		scheduleDao.insert(schedule);

		schedule = new Schedule();
		schedule.setSeats(6);
		schedule.setRoute(template4);
		schedule.setDateStart(formatDate.parse("09.05.2014"));
		scheduleDao.insert(schedule);
		
		trans.commit();
		
	}
}
