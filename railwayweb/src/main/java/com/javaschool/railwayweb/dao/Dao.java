package com.javaschool.railwayweb.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

public abstract class Dao <T extends Serializable> {
	private Class<T> entityClass;
	
	protected void setEntityClass(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	@PersistenceContext
	public EntityManager em;
	
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
	@Transactional	
	public void insert(T obj) {
		em.persist(obj);
	}
	@Transactional	
	public void update(T obj) {
		this.insert(obj);
	}
	
	public T find(long id) {
		return em.find(entityClass, id);
	}
	@Transactional
	public void delete(T obj) {
		em.remove(em.contains(obj) ? obj : em.merge(obj));
	}
	
	public List<T> getAll() {
		List<T> ret = em.createQuery("select t from " + entityClass.getName() + " t", entityClass).getResultList();
		return ret;
	}
}
