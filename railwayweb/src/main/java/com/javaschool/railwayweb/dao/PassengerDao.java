package com.javaschool.railwayweb.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.javaschool.railwayweb.model.*;

@Service
public class PassengerDao extends Dao<Passenger> {	
	public PassengerDao() {
		super();
		setEntityClass(Passenger.class);
	}
	
	public Passenger findByNameDate(String firstName, String lastName, Date birthdate) {
		TypedQuery<Passenger> q = em.createQuery("select p from Passenger p"
				+ " where p.firstname = :firstname"
				+ " and p.lastname = :lastname"
				+ " and p.birthdate = :birthdate", Passenger.class);
		q.setParameter("firstname", firstName);
		q.setParameter("lastname", lastName);
		q.setParameter("birthdate", birthdate);
		List<Passenger> list = q.getResultList();
		Passenger ret = null;
		if (list.size() == 1)
			ret = list.get(0);
		else if (list.size() >= 2) {
			// TODO Error
		}
		return ret;
	}
	
	public Passenger registerPassenger(Passenger p) {
		Passenger passenger = this.findByNameDate(p.getFirstname(), p.getLastname(), p.getBirthdate());
		if (passenger == null) {
			passenger = p;
			insert(passenger);
		}
		return passenger;
	}
	
}
