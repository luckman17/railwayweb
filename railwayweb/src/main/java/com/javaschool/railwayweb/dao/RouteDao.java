package com.javaschool.railwayweb.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javaschool.railwayweb.model.Route;
import com.javaschool.railwayweb.model.RouteLines;

@Service
public class RouteDao extends Dao<Route>{
	@Autowired
	private RouteLinesDao routeLinesDao;
	public RouteDao() {
		super();
		setEntityClass(Route.class);
	}
	public void insertWithLines(Route route) {
		em.persist(route);
		if (route.getRouteLines() != null) {
			for (RouteLines rl : route.getRouteLines()) {
				rl.getKey().setRoute(route);
				routeLinesDao.insert(rl);
			}
		}
	}
	
}
