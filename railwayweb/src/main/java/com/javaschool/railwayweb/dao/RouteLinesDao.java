package com.javaschool.railwayweb.dao;

import org.springframework.stereotype.Service;

import com.javaschool.railwayweb.model.RouteLines;

@Service
public class RouteLinesDao extends Dao<RouteLines> {
	public RouteLinesDao() {
		super();
		setEntityClass(RouteLines.class);
	}
}
