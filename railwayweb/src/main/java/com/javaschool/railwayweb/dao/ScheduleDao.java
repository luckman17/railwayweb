package com.javaschool.railwayweb.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.javaschool.railwayweb.model.*; 

@Service
public class ScheduleDao extends Dao<Schedule> {
	
	public ScheduleDao() {
		super();
		setEntityClass(Schedule.class);
	}
	
	public Schedule findByRouteDate(Route route, Date date) {
		TypedQuery<Schedule> q = em.createQuery("select s from Schedule s"
				+ " where s.route = :route"
				+ " and s.dateStart = :dateStart", Schedule.class);
		q.setParameter("route", route);
		q.setParameter("dateStart", date);
		List<Schedule> list = q.getResultList();
		Schedule ret = null;
		if (list.size() == 1)
			ret = list.get(0);
		return ret;
	}
	
	public List<Schedule> getFilteredSchedule(ScheduleFilter sf) {
		em.clear();
		Date date = sf.dateStart;
		List<Schedule> schedules = null;
		if (date != null) {
			TypedQuery<Schedule> query = em.createQuery(
					"select s from Schedule s where s.dateStart = :dateStart", Schedule.class);
			schedules = query.setParameter("dateStart", date).getResultList();
		}
		else {
			schedules = em.createQuery("select s from Schedule s order by s.dateStart", Schedule.class).getResultList();
		}
		
		List<Schedule> ret = new ArrayList<Schedule>();
		
		for (Schedule s : schedules) {
			List<RouteLines> lines = s.getRoute().getRouteLines();
			boolean firstStation = false;
			boolean secondStation = false;
			if (sf.stationIdFrom == 0)
				firstStation = true;
			if (sf.stationIdTo == 0)
				secondStation = true;
			int len = lines.size();
			for (int j = 0; j < len; j++) {
				RouteLines rl = lines.get(j);
				if (!firstStation && j < len - 1 && rl.getStation().getId() == sf.stationIdFrom)
					firstStation = true;
				if (firstStation && j > 0 && rl.getStation().getId() == sf.stationIdTo)
					secondStation = true;
			}
			if (firstStation && secondStation) {
				s.getTickets();
				ret.add(s);
			}
		}
		return ret;
	}
	public List<Passenger> getPassengerList(Schedule s) {
		TypedQuery<Passenger> query = em.createQuery(
					"select p from Ticket t join t.passenger p"
				+ " where t.schedule = :schedule", Passenger.class);
		query.setParameter("schedule", s);
		List<Passenger> ret = query.getResultList();
		return ret;
	}
	
	public int getTicketsLeft(Schedule s) {
		String queryString = "Select count(t) from Ticket t"
				+ " where t.schedule = :schedule";
		Query queryCount = em.createQuery(queryString);
		queryCount.setParameter("schedule", s);
		int ticketsSell = ((Long) queryCount.getSingleResult()).intValue();
		return s.getSeats() - ticketsSell;
	}
}
