package com.javaschool.railwayweb.dao;

import org.springframework.stereotype.Service;

import com.javaschool.railwayweb.model.Station;

@Service
public class StationDao extends Dao<Station>{
	StationDao() {
		super();
		setEntityClass(Station.class);
	}
	public Station findByName(String name) {
		Station ret = em.createQuery("select s from Station s where s.name = :name",
				Station.class).setParameter("name", name).getSingleResult();
		return ret;
	}	
}
