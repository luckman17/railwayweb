package com.javaschool.railwayweb.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.springframework.stereotype.Service;

import com.javaschool.railwayweb.model.*;

@Service
public class TicketDao extends Dao<Ticket> {
	TicketDao() {
		super();
		setEntityClass(Ticket.class);		
	}
	
	public boolean haveTicket(Passenger p, Schedule s) {
		TypedQuery<Ticket> query = em.createQuery("select t from Ticket t"
				+ " where t.passenger = :passenger"
				+ " and t.schedule = :schedule", Ticket.class);
		query.setParameter("passenger", p);
		query.setParameter("schedule", s);
		List<Ticket> list = query.getResultList();	
		return !list.isEmpty();
	}
}
