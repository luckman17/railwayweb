package com.javaschool.railwayweb.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.javaschool.railwayweb.model.User;

@Service
public class UserDao extends Dao<User> {
	UserDao() {
		super();
		setEntityClass(User.class);
	}
	
	public User findByLogin(String login) {
		TypedQuery<User> query = em.createQuery("select u from User u"
				+ " where u.login = :login", User.class);
		query.setParameter("login", login);
		List<User> list = query.getResultList();
		if (list.isEmpty())
			return null;
		else
			return list.get(0);
	}
}
