package com.javaschool.railwayweb.dto;

import javax.validation.Valid;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.javaschool.railwayweb.model.Passenger;

@Component
@Scope("prototype")
public class BuyTicketDto {
	@Valid
	private Passenger passenger;
	private long scheduleId;
	private int pointFrom;
	private int pointTo;
	
	public Passenger getPassenger() {
		return passenger;
	}
	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}
	public long getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(long scheduleId) {
		this.scheduleId = scheduleId;
	}
	public int getPointFrom() {
		return pointFrom;
	}
	public void setPointFrom(int pointFrom) {
		this.pointFrom = pointFrom;
	}
	public int getPointTo() {
		return pointTo;
	}
	public void setPointTo(int pointTo) {
		this.pointTo = pointTo;
	}
	
	
}
