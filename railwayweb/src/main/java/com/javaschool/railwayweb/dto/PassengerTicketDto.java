package com.javaschool.railwayweb.dto;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.javaschool.railwayweb.dao.PassengerDao;
import com.javaschool.railwayweb.model.Passenger;
import com.javaschool.railwayweb.model.RouteLines;
import com.javaschool.railwayweb.model.Ticket;

@Component
@Scope("prototype")
public class PassengerTicketDto {
	private String firstname;
	private String lastname;
	@DateTimeFormat(pattern="dd.MM.yyyy")
	private Date birthdate;
	private String stationFrom;
	private String stationTo;
	
	public PassengerTicketDto() {		
	}
	
	public PassengerTicketDto(Ticket t) {
		Passenger p = t.getPassenger();
		this.firstname = p.getFirstname();
		this.lastname = p.getLastname();
		this.birthdate = p.getBirthdate();
		List<RouteLines> rl = t.getSchedule().getRoute().getRouteLines();
		this.stationFrom = rl.get(t.getPointNumFrom() - 1).getStation().getName();
		this.stationTo = rl.get(t.getPointNumTo() - 1).getStation().getName();
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public String getStationFrom() {
		return stationFrom;
	}
	public void setStationFrom(String stationFrom) {
		this.stationFrom = stationFrom;
	}
	public String getStationTo() {
		return stationTo;
	}
	public void setStationTo(String stationTo) {
		this.stationTo = stationTo;
	}
	
	
}
