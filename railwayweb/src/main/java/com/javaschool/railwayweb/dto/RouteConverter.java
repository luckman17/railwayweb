package com.javaschool.railwayweb.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.javaschool.railwayweb.dao.RouteDao;
import com.javaschool.railwayweb.model.Route;

@Component
public class RouteConverter implements Converter<String, Route>{
	
	@Autowired
	RouteDao routeDao;

	public Route convert(String s) {
		return routeDao.find(Long.parseLong(s));
	}

}
