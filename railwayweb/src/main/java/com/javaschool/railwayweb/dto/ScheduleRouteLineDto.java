package com.javaschool.railwayweb.dto;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ScheduleRouteLineDto {
	private String stationName;
	private Date passageDateTime;
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	public Date getPassageDateTime() {
		return passageDateTime;
	}
	public void setPassageDateTime(Date passageDateTime) {
		this.passageDateTime = passageDateTime;
	}

}
