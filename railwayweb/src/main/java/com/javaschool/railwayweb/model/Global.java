package com.javaschool.railwayweb.model;

import java.text.SimpleDateFormat;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;


public final class Global {
	private Global() {}
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	public static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	public static final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
	public static final Validator validator = validatorFactory.getValidator();	
}
