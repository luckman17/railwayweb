package com.javaschool.railwayweb.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Route implements Serializable {
	private static final long serialVersionUID = 2658788702421739720L;
	@Id
	@GeneratedValue
	private long id;
	@Column
	@NotNull(message = "Route name should be non-empty")
	@Size(min = 4, max = 50, message = "Name length must be from 4 to 50")
	private String routeName;

	@OneToMany (mappedBy = "key.route", fetch = FetchType.EAGER)
	private List<RouteLines> routeLines;
	
	public List<RouteLines> getRouteLines() {
		return routeLines;
	}
	public void setRouteLines(List<RouteLines> routeLines) {
		this.routeLines = routeLines;
	}

	public String getRouteName() {
		return routeName;
	}
	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}
	
	public long getId() {
		return id;
	}
	public String toString() {
		return id + ". " + routeName; 
	}
	
	public Station getStationTo() {
		return routeLines.get(routeLines.size() - 1).getStation();
	}
	
}
