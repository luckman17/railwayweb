package com.javaschool.railwayweb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;

@Embeddable
public class RouteLinesPK implements Serializable {
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "RouteId")
	private Route route;
	@Column
	@Min(1)
	private int pointNum;
	
	public RouteLinesPK() {
		
	}
	public RouteLinesPK(Route route, int pointNum) {
		this.route = route;
		this.pointNum = pointNum; 
	}
	
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public int getPointNum() {
		return pointNum;
	}
	public void setPointNum(int pointNum) {
		this.pointNum = pointNum;
	}
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o instanceof RouteLinesPK) {
			RouteLinesPK rlPk = (RouteLinesPK)o;
			return rlPk.getRoute().getId() == this.getRoute().getId()
				&& rlPk.getPointNum() == this.getPointNum();
		}
		else {
			return false;
		}
	}
	@Override
	public int hashCode() {
		return (int)getRoute().getId() * 37 + pointNum;
	}
	
}
