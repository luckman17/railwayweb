package com.javaschool.railwayweb.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(uniqueConstraints = {
		@UniqueConstraint(columnNames = {"TemplateId", "dateStart"})
})
public class Schedule implements Serializable {
	private static final long serialVersionUID = -222742999167295180L;
	@Id
	@GeneratedValue
	private long id;
	@Column
	@Min(value = 1, message = "Seats num must be greater than or equal to 1")
	private int seats;
	
	@ManyToOne
	@JoinColumn(name = "TemplateId")
	private Route route;
	@Column
	@Temporal(TemporalType.DATE)
	@NotNull(message = "Start date is mandatory field")
	@DateTimeFormat(pattern = "dd.MM.yyyy")	
	private Date dateStart;
	
	@OneToMany(mappedBy="schedule")
	private Set<Ticket> tickets;
	
	public Set<Ticket> getTickets() {
		return tickets;
	}
	public void setTickets(Set<Ticket> tickets) {
		this.tickets = tickets;
	}
	
	public long getId() {
		return id;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}	
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public Date getDateStart() {
		return dateStart;
	}
	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	
	// TODO add comment
	public Date getStationDateTime(Station s) {
		long stationId = 0;
		if (s != null)
			stationId = s.getId();
		DateTime startDt = new DateTime(dateStart);
		Date lastTime = null;
		for (RouteLines rl : route.getRouteLines()) {
			if (lastTime != null) {
				long delta = rl.getTimePass().getTime() - lastTime.getTime();
				if (delta < 0)
					delta += 24 * 3600 * 1000;
				startDt = startDt.plus(new Duration(delta));
			}
			else {
				LocalTime departTime = new LocalTime(rl.getTimePass());
				startDt = startDt.plus(departTime.getMillisOfDay());
			}
			lastTime = rl.getTimePass();
			if (rl.getStation().getId() == stationId)
				break;
		}
		return startDt.toDate();
	}
	
	public Date getStationToDateTime() {
		return this.getStationDateTime(null);
	}
	
	public Date getStationFromDateTime() {
		return this.getStationDateTime(route.getRouteLines().get(0).getStation());	
	}
	
	
	public int getEmptySeats() {		
		int ticketsSell = tickets.size();
		return seats - ticketsSell;
	}
	
	public boolean haveTimeToBuy() {
		DateTime currentDT = new DateTime();
		DateTime departDT = new DateTime(dateStart);
		LocalTime departTime = new LocalTime(route.getRouteLines().get(0).getTimePass());
		departDT = departDT.plus(departTime.getMillisOfDay());
		Duration duration = new Duration(currentDT, departDT);
		if (duration.isShorterThan(new Duration(10 * 60 * 1000L)))
			return false;
		return true;		
	}
}

