package com.javaschool.railwayweb.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ScheduleFilter implements Serializable {
	private static final long serialVersionUID = 345441016991304225L;
	@DateTimeFormat(pattern = "dd.MM.yyyy")
	public Date dateStart;
	public long stationIdFrom;
	public long stationIdTo;
	
	public Date getDateStart() {
		return dateStart;
	}
	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	public long getStationIdFrom() {
		return stationIdFrom;
	}
	public void setStationIdFrom(long stationIdFrom) {
		this.stationIdFrom = stationIdFrom;
	}
	public long getStationIdTo() {
		return stationIdTo;
	}
	public void setStationIdTo(long stationIdTo) {
		this.stationIdTo = stationIdTo;
	}
	@Override
	public String toString() {
		String ret;
		if (dateStart == null)
			ret = "null";
		else
			ret = Global.dateFormat.format(dateStart);
		ret = "[dateStart: " + ret;
		return ret +
				", stationFrom: " + stationIdFrom +
				", stationTo: " + stationIdTo + "]";
	}
}
