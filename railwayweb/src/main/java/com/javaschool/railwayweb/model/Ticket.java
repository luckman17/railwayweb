package com.javaschool.railwayweb.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Ticket implements Serializable{
	private static final long serialVersionUID = 2L;
	@Id
	@GeneratedValue
	private long id;
	@ManyToOne
	@JoinColumn(name = "ScheduleId")
	private Schedule schedule;
	@ManyToOne
	@JoinColumn(name = "PassengerId")
	private Passenger passenger;
	
	@Column
	private int pointNumFrom;
	@Column
	private int pointNumTo;
	
	public long getId() {
		return id;
	}
	public Schedule getSchedule() {
		return schedule;
	}
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	public Passenger getPassenger() {
		return passenger;
	}
	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}
	public int getPointNumFrom() {
		return pointNumFrom;
	}
	public void setPointNumFrom(int pointNumFrom) {
		this.pointNumFrom = pointNumFrom;
	}
	public int getPointNumTo() {
		return pointNumTo;
	}
	public void setPointNumTo(int pointNumTo) {
		this.pointNumTo = pointNumTo;
	}
	
}
