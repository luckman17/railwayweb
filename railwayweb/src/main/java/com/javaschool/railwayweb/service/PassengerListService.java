package com.javaschool.railwayweb.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javaschool.railwayweb.dao.PassengerDao;
import com.javaschool.railwayweb.dao.ScheduleDao;
import com.javaschool.railwayweb.dao.TicketDao;
import com.javaschool.railwayweb.dto.PassengerTicketDto;
import com.javaschool.railwayweb.model.Schedule;
import com.javaschool.railwayweb.model.Ticket;

@Service
public class PassengerListService {
	@Autowired
	ScheduleDao scheduleDao;
	@Autowired
	TicketDao ticketDao;
	@Autowired
	PassengerDao passengerDao;
	public List<PassengerTicketDto> getPassengerList(int scheduleId) {
		Schedule schedule = scheduleDao.find(scheduleId);
		Set<Ticket> ticketSet = schedule.getTickets();
		List<PassengerTicketDto> ret = new ArrayList<PassengerTicketDto>();
		for (Ticket t : ticketSet) {
			ret.add(new PassengerTicketDto(t));
		}
		return ret;
	}
}
