package com.javaschool.railwayweb.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javaschool.railwayweb.dao.RouteDao;
import com.javaschool.railwayweb.dao.StationDao;
import com.javaschool.railwayweb.model.Route;
import com.javaschool.railwayweb.model.RouteLines;
import com.javaschool.railwayweb.model.Station;

@Service
public class RouteService {
	
	@Autowired
	StationDao stationDao;
	@Autowired
	RouteDao routeDao;
	@Autowired
	ValidatorService validatorService;
	
	@Transactional
	public String addRoute(String routeName, String jsonData) {
		if (jsonData.charAt(0) == '"')
			jsonData = jsonData.substring(1, jsonData.length() - 1);
		Route route = new Route();
		String ret = "";
		List<RouteLines> routeLinesList = new ArrayList<RouteLines>();
		route.setRouteName(routeName);
		JSONArray jLines = new JSONArray(jsonData);
		for (int i = 0; i < jLines.length(); i++) {
			if (i == 0)
				continue;
			JSONObject line = jLines.getJSONObject(i);
			RouteLines routeLine = new RouteLines();
			routeLine.getKey().setPointNum(i);
			routeLine.setStation(stationDao.find( (long)(line.getLong("Station id")) ));
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			try {
				routeLine.setTimePass(sdf.parse(line.getString("Time")) );
			} catch (ParseException e) {
				ret = "Wrong time: " + line.getString("Time");
			}
			routeLinesList.add(routeLine);
		}
		if (routeLinesList.size() < 2) {
			ret = "Route cannot contain less than 2 stations!";
		}
		if (!ret.equals(""))
			return ret;
		route.setRouteLines(routeLinesList);
		String validationStr = validatorService.validate(route);
		if (!validationStr.equals(""))
			return validationStr;
		routeDao.insertWithLines(route);
		return ret;
	}
	
	public List<Station> getStationList(Route r) {
		List<RouteLines> routeLinesList = r.getRouteLines();
		List<Station> ret = new ArrayList<Station>();
		for (RouteLines rl : routeLinesList) {
			ret.add(rl.getStation());
		}
		return ret;
	}
	
	public Map<Integer, Station> getStationMap(Route r) {
		Map<Integer, Station> stationMap = new LinkedHashMap<Integer, Station>();
		List<RouteLines> rlList = r.getRouteLines();
		for (int i = 0; i < rlList.size(); i++) {
			stationMap.put(i + 1, rlList.get(i).getStation());
		}
		return stationMap;
	}
	
	public Station getStation(Route r, int pointNum) {
		return r.getRouteLines().get(pointNum - 1).getStation(); 
	}
	
}
