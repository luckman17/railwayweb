package com.javaschool.railwayweb.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javaschool.railwayweb.dao.RouteDao;
import com.javaschool.railwayweb.dao.ScheduleDao;
import com.javaschool.railwayweb.dto.ScheduleRouteLineDto;
import com.javaschool.railwayweb.model.Route;
import com.javaschool.railwayweb.model.RouteLines;
import com.javaschool.railwayweb.model.Schedule;
import com.javaschool.railwayweb.model.Station;
import com.javaschool.railwayweb.model.Ticket;

@Service
public class ScheduleService {
	@Autowired
	RouteDao routeDao;
	@Autowired
	ScheduleDao scheduleDao;
	@Autowired
	ValidatorService validatorService;
	
	@Transactional
	public String addSchedule(Route r, Date startDate, int seats) {
		String ret = "";
		if (startDate == null) {
			return "Wrong date!";
		}
		Schedule schedule = new Schedule();
		schedule.setRoute(r);
		schedule.setDateStart(startDate);
		schedule.setSeats(seats);
		Date yesterday = new Date(new Date().getTime() - 24 * 3600 * 1000);
		if (startDate.before(yesterday)) {
			ret = "You can only add schedule to future dates";
		}
		if (ret.equals(""))
			ret = validatorService.validate(schedule);
		if (ret.equals("")) {
			// find same schedule
			Schedule copy = scheduleDao.findByRouteDate(r, startDate);
			if (copy != null) {
				ret = "Schedule with same Route and Start Date already exists.";
			}
		}
		if (ret.equals(""))
			scheduleDao.insert(schedule);
		return ret;
	}
	
	public int getTicketsLeft(Schedule s, int pointFrom, int pointTo) {
		Set<Ticket> tickets = s.getTickets();
		int len = s.getRoute().getRouteLines().size();
		int[] openPoints = new int[len + 1];
		int[] closePoints = new int[len + 1];
		for (Ticket t : tickets) {
			openPoints[t.getPointNumFrom()]++;
			closePoints[t.getPointNumTo()]++;
		}
		int opened = 0, closed = 0;
		int mi = 0;
		for (int i = 1; i <= len; i++) {
			opened += openPoints[i];
			closed += closePoints[i];
			if (i >= pointFrom && i <= pointTo) {
				if (mi < opened - closed)
					mi = opened - closed;
			}
		}
		return s.getSeats() - mi;
	}
	
	public List<ScheduleRouteLineDto> getScheduleRouteLines(long id) {
		List<ScheduleRouteLineDto> ret = new ArrayList<ScheduleRouteLineDto>();
		Schedule schedule = scheduleDao.find(id);
		List<RouteLines> rlList = schedule.getRoute().getRouteLines();
		for (int i = 0; i < rlList.size(); i++) {
			ScheduleRouteLineDto srlDto = new ScheduleRouteLineDto();
			Station station = rlList.get(i).getStation();
			srlDto.setStationName(station.getName());
			srlDto.setPassageDateTime(schedule.getStationDateTime(station));
			ret.add(srlDto);
		}		
		return ret;
	}
}
