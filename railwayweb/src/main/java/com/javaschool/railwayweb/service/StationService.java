package com.javaschool.railwayweb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javaschool.railwayweb.dao.StationDao;
import com.javaschool.railwayweb.model.Station;

@Service
public class StationService {
	@Autowired
	StationDao stationDao;
	
	
	public String deleteStation(long id) {
		String ret = "";
		Station s = stationDao.find(id);
		if (s != null) {
			try {				
				stationDao.delete(s);
			}
			catch (Exception e) {
				ret = "Error: cannot delete station!";
			}
		}
		else {
			ret = "Cannot find station with id = " + id;
		}
		return ret;
	}
}
