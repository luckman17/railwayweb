package com.javaschool.railwayweb.service;

import java.util.List;

import javax.persistence.TypedQuery;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javaschool.railwayweb.dao.PassengerDao;
import com.javaschool.railwayweb.dao.ScheduleDao;
import com.javaschool.railwayweb.dao.TicketDao;
import com.javaschool.railwayweb.model.Passenger;
import com.javaschool.railwayweb.model.Schedule;
import com.javaschool.railwayweb.model.Ticket;

@Service
public class TicketService {
	@Autowired
	PassengerDao passengerDao;
	@Autowired
	TicketDao ticketDao;
	@Autowired
	ScheduleDao scheduleDao;
	@Autowired
	ScheduleService scheduleService;
	
	public String checkPoints(Schedule s, int pointFrom, int pointTo) {
		int totalPoints = s.getRoute().getRouteLines().size();
		if (pointFrom < 1) {
			return "Wrong station from";
		}
		if (pointTo > totalPoints) {
			return "Wrong station to";
		}
		if (pointTo < pointFrom) {
			return "Wrong station order";
		}
		if (pointTo == pointFrom) {
			return "Stations can't be identical";
		}
		if (scheduleService.getTicketsLeft(s, pointFrom, pointTo) <= 0) {
			return "Sorry but there is no tickets left";
		}
		return "";
	}
		
	/**
	 * @return error String if unable to buy a ticket, empty String otherwise
	 */
	@Transactional
	public String buyTicket(Passenger p, Schedule s, int pointFrom, int pointTo) {
		String ret = "";
		p = passengerDao.registerPassenger(p);
		if (ticketDao.haveTicket(p, s)) {
			ret = "Unable to buy ticket because this passenger already have ticket on this train";
		}
		else {		
			// check empty seats
			if (scheduleService.getTicketsLeft(s, pointFrom, pointTo) <= 0) {
				ret = "Sorry, but the train is already full"; 
			}
			else {
				// check depart time
				DateTime currentDT = new DateTime();
				DateTime departDT = new DateTime(s.getDateStart());
				LocalTime departTime = new LocalTime(s.getRoute().getRouteLines().get(0).getTimePass());
				departDT = departDT.plus(departTime.getMillisOfDay());
				Duration duration = new Duration(currentDT, departDT);
				if (duration.isShorterThan(new Duration(10 * 60 * 1000L)))
					ret = "The train leaves in less than 10 minutes";
			}
		}
		
		if (ret.equals("")) {
			Ticket t = new Ticket();
			t.setPassenger(p);
			t.setSchedule(s);
			t.setPointNumFrom(pointFrom);
			t.setPointNumTo(pointTo);
			ticketDao.insert(t);
		}
			
		return ret;
	}
}
