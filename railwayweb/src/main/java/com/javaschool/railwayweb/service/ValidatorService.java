package com.javaschool.railwayweb.service;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidatorService {
	@Autowired
	Validator validator;
	
	public String validate(Object obj) {
		String ret = "";
		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(obj);
		
		if (!constraintViolations.isEmpty()) {
			ret += constraintViolations.size() + " error(s):\n";
		}
		for (ConstraintViolation<Object> cv : constraintViolations) {
			ret += cv.getMessage() + "\n";
		}
		return ret;
	}
}
