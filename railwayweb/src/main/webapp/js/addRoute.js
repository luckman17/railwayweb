function addStation() {
	var stationList = [];
	$("#routeLinesTable tbody tr td:first-child").each(function() {
		stationList.push($(this).text());
	});
	
	var selectedStation = $("#stationSelect").val(); 
	
	if ($.inArray(selectedStation, stationList) >= 0) {
		$("#errorDiv").text("This station is already in route!");
		return false;
	}
	else {
		$("#errorDiv").text("");
	}
	
	$("#routeLinesTable tr:last").after(
			"<tr>" +
			"<td>" + $("#stationSelect").val() + "</td>" +
			"<td>" + $("#stationSelect option:selected").text() + "</td>" +
			"<td>" + $("#timeInput").val() + "</td>" +
			"<td>" + '<a class="deleteRoute" href=""> <img src="/railwayweb/img/delete-button.png" height="30"/></a>' + "</td>" +
			"</tr>");
	$("#routeLinesTable .deleteRoute").on("click",function() {
        var tr = $(this).closest('tr');
        tr.css("background-color","#FF4720");
        tr.fadeOut(600, function(){
            tr.remove();
        });
        return false;
    });
	$("#timeInput").val("");
	$("#routeLinesTable").tableDnD();
}
function sendRoute() {
	/*
	if ($("#routeName").val().length < 4) {
		$("#errorDiv").text("Name length should be from 4 to 50.");
	}
	*/
	var tbl = $("#routeLinesTable").tableToJSON();
	/*
	$.ajax({
		  type: "POST",
		  url: "send/",
		  contentType: "application/json",
		  dataType: "text",
		  data: {routeLines: JSON.stringify(tbl), routeName: $("#routeName").val()},
		  success: function(data) {
			  window.location.replace(data);
		  }
		});
	*/
		
	$.post("send/", {routeLines: JSON.stringify(tbl), routeName: $("#routeName").val()},
			function(data) {
				var chars = data.substring(0, 4);
				if (chars == "http") {
					window.location.replace(data);
				}
				else {
					$("#errorDiv").html(data.replace(/\n/g, "<br />"));
				}					
			},
			"text");
	
}

function getJsonString() {
	var tbl = $("#routeLinesTable").tableToJSON();
	return JSON.stringify(tbl);
}

$.validator.addMethod("timeInput", function(value, element) {  
	return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])?$/i.test(value);  
	}, "Please enter a valid time.");


$(document).ready(function(){
	$(".nav-sidebar li").removeClass("active");
	$("#routeMenu").addClass("active");

	$("#addStationForm").validate({
        rules: {
        	timeInput: {
        		required: true,
        		minlength: 5,
        		maxlength: 5,
        		timeInput: true
        	}        		
        },
        messages: {
        	timeInput: "Wrong time format! <br> Valid is hh:mm"
        }
	});
});
