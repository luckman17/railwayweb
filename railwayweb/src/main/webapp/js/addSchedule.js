$.validator.addMethod("dateFormat", function(value, element) {  
	return this.optional(element) || /^(\d{1,2}).(\d{1,2}).(\d{4})$/i.test(value);  
	}, "Wrong date format!");

$(document).ready(function(){
	$(".nav-sidebar li").removeClass("active");
	$("#addScheduleMenu").addClass("active");
	$("#scheduleForm .input-group.date").datepicker({format: "dd.mm.yyyy"});
	
	$("#scheduleForm").validate({
        rules: {
        	seats: {
        		number: true
        	},
        	dateStart: {
        		required: true,
        		dateFormat: true
        	}
        },
        messages: {
        	seats: "Seats num must be a number!",
        	dateStart: "Wrong date format!"
        }
	});
	
});
