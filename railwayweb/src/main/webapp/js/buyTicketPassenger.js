$.validator.addMethod("passBirthdate", function(value, element) {  
	return this.optional(element) || /^(\d{1,2}).(\d{1,2}).(\d{4})$/i.test(value);  
	}, "Please enter a valid birthdate.");


$(document).ready(function(){
	$("#buyTicketPassengerForm").validate({
        rules: {
        	"passenger.firstname": {
        		required: true,
        		minlength: 2,
        		maxlength: 50,
        	},
			"passenger.lastname": {
				required: true,
				minlength: 2,
				maxlength: 50
			},
			"passenger.birthdate": {
				passBirthdate: true
			}
        },
        messages: {
        	"passenger.firstname": "First name length must be from 2 to 50",
        	"passenger.lastname": "Last name length must be from 2 to 50",
        	"passenger.birthdate": "Wrong birthdate format"
        }
	});
});
