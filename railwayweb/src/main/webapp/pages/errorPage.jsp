<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page isErrorPage="true"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet">
</head>
<body>


<div>

	<div class="col-md-10 col-md-offset-1" style="padding-top: 100px">
		<h2>Something went wrong...</h2>
		<strong>Sorry, an error occurred.</strong>
		<br><br>
		
		<div class="spoiler">
		    <div class="btn spoiler-btn">Stack Trace</div>
		    <div class="spoiler-body collapse">
				Exception:  ${exception.message}
			    <c:forEach items="${exception.stackTrace}" var="ste">
			    	${ste} 
			    </c:forEach>
		    </div>
		</div>
		
    </div>

<spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
<script src="${jqueryUrl}"></script>
<spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
<script src="${bootstrapJsUrl}"></script>
<spring:url value="/js/errorPage.js" var="errorPageJs"></spring:url>
<script type="text/javascript" src="${errorPageJs}"></script>

</body>
</html>