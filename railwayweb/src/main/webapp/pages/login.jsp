<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet">
	<spring:url value="/css/login.css" var="loginCss"></spring:url>    
	<link href="${loginCss}" rel="stylesheet">	
</head>
<body>


<div>
	<div id="loginContainer" class="col-md-3">
		<c:if test="${not empty param.error}">
			<div class="alert alert-danger">Login error: 
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</div>
		</c:if>
		<jsp:include page="loginForm.jsp"></jsp:include>
	</div>
</div>

</body>
</html>