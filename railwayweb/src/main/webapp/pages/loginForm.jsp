<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form method="POST" action="<c:url value="/j_spring_security_check" />" class="form-horizontal">
	<div class="form-group">
			<label for="loginInput">Login:</label>
			<input name="j_username" class="form-control" id="loginInput" />
	</div>
	<div class="form-group">
		<label for="passwordInput">Password:</label>
		<input type="password" name="j_password" class="form-control"/>
	</div>
	<div class="form-group">
		<input type="submit" value="Login" class="btn btn-primary"/>
	</div>
</form>