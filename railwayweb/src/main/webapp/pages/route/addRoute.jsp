<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
  <head>
    <title>Add route</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet"/>
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">
	<spring:url value="/css/rtable.css" var="rTableCss"></spring:url>
	<link href="${rTableCss}" rel="stylesheet">	
	<spring:url value="/css/addRoute.css" var="addRouteCssUrl"></spring:url>
	<link href="${addRouteCssUrl}" rel="stylesheet">
  </head>

  <body>
    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="margin-top: 30px">
			<div class="rheader">
				<div id="addRouteGroup" class="form-inline rheader-buttons">
					<div class="form-group">
						<table> <tr>
							<td> Route name: </td>
							<td><input id="routeName" class="form-group form-control"/></td>
						</tr> </table>
					</div>
					<div class="form-group">
						<a href="javascript:sendRoute()" class="btn btn-primary">Add route</a>
					</div>
				</div>
		        	<div id="errorDiv" class="error rheader-message">
					</div>				
			</div>

			<div class="col-sm-8" id="tableContainer">
				<table id="routeLinesTable" class="table rtable">
					<thead>
						<tr>
							<th>Station id</th>
							<th>Station</th>
							<th>Time</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
						</tr>
					</tbody>
				</table>
			</div>
<!--
			<div class="form pull-right" style="margin: 30px 50px">			
				<select id="stationSelect" class="form-group form-control">
					<c:forEach var="station" items="${stationList}">
						<option value="${station.id}">${station.name}</option>
					</c:forEach>
				</select>
				<div class="form-group">
					<div class="text">Time:</div>
					<input id="timeInput" class="form-group form-control"/>
				</div>								
				<div class="form-group">
					<a href="javascript:addStation()" class="btn btn-primary">Add station</a>
				</div>
			</div>
-->
			<form action="javascript:addStation()" class="form pull-right" id="addStationForm" style="margin: 30px 50px">
				<fieldset>			
				<select id="stationSelect" class="form-group form-control">
					<c:forEach var="station" items="${stationList}">
						<option value="${station.id}">${station.name}</option>
					</c:forEach>
				</select>
				<div class="form-group">
					<div class="text">Time:</div>
					<input id="timeInput" name="timeInput" class="form-group form-control"/>
				</div>								
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Add station">
				</div>
				</fieldset>
			</form>
       
        </div>	<!-- main -->        
        
      </div>	<!-- row -->
    </div>		<!-- container-fluid -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
    <script src="${jqueryUrl}"></script>
    <spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
    <script src="${bootstrapJsUrl}"></script>
    <spring:url value="/js/jquery.validate.js" var="jqueryValidator"></spring:url>
    <script type="text/javascript" src="${jqueryValidator}"></script>
 	<spring:url value="/js/jquery.tabletojson.min.js" var="tableToJsonJs"></spring:url>
 	<script type="text/javascript" src="${tableToJsonJs}"></script>
 	<spring:url value="/js/tablednd.js" var="tableDndJs"></spring:url>
 	<script type="text/javascript" src="${tableDndJs}"></script>
    <spring:url value="/js/addRoute.js" var="addRouteJs"></spring:url>
 	<script type="text/javascript" src="${addRouteJs}"></script>
  </body>
</html>
