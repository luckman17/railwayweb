<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<!DOCTYPE html>
<html>
  <head>
    <title>Routes</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>
	<link href="${bootstrapMinUrl}" rel="stylesheet"/>
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">
	<spring:url value="/css/rtable.css" var="rTableCss"></spring:url>
	<link href="${rTableCss}" rel="stylesheet">
  </head>

  <body>
    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
			<div class="rheader col-md-6">
				<div class="rheader-buttons">
					<spring:url value="/route/add" var="addRouteUrl"></spring:url>
					<a href="${addRouteUrl}" id="addRouteBtn" role="button" class="btn btn-primary">Add route</a>
				</div>
        	</div>
        	
        	<div id="tableContainer">
				<jsp:include page="routeTable2.jsp"></jsp:include>
			</div>        	
        </div>	<!-- main -->        
        
      </div>	<!-- row -->
    </div>		<!-- container-fluid -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
    <script src="${jqueryUrl}"></script>
    <spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
    <script src="${bootstrapJsUrl}"></script>
    <spring:url value="/js/routeList.js" var="routeListJs"></spring:url>
 	<script type="text/javascript" src="${routeListJs}"></script>
    
  </body>
</html>
