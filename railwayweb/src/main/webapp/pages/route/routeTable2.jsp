<%@ page language="java" contentType="text/html"%>
<!DOCTYPE html>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


				<table class="table rtable">
					<tr>
						<th class="col-md-2 text-center">Route id</th>
						<th>Route name</th>
						<th>Station from</th>
						<th>Station to</th>
					</tr>
					<c:forEach items = "${routeList}" var = "line">
						<tr>
							<td class="text-center">${line.id}</td>
							<td>${line.routeName}</td>
							<td>${line.routeLines.get(0).station.name}</td>
							<td>${line.stationTo.name}</td>
						</tr>
					</c:forEach>
				</table>
