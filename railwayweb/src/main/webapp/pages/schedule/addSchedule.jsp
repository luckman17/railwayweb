<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
  <head>
    <title>Add schedule</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet"/>
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">
	<spring:url value="/css/datepicker3.css" var="datePickerCssUrl"></spring:url>
	<link href="${datePickerCssUrl}" rel="stylesheet">	
	<spring:url value="/css/addSchedule.css" var="addScheduleUrl"></spring:url>
	<link href="${addScheduleUrl}" rel="stylesheet"/>
  </head>

  <body>
    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<div class="rheader col-md-8">
	        	<c:if test="${not empty msg}">
		        	<div class="alert alert-danger rheader-message">
		        		<a class="close" data-dismiss="alert">�</a>
						${msg}
					</div>
				</c:if>
        	</div>        
        
             <form:form method="post" modelAttribute="schedule" class="form-horizontal col-md-3 col-sm-4" id="scheduleForm">
          		<fieldset>
             	<div>
						<div class="form-group">
							<label for="routeSelect">Route: </label>
								<form:select path="route" class="form-control" id="routeSelect">
									<form:options items="${routeList}" itemLabel="routeName" itemValue="id"/>
								</form:select>
						</div>
						<div class="form-group">
							<label for="dateInputGroup">Date start: </label>
							<div class="input-group date" id="dateInputGroup">
							  <form:input path="dateStart" type="text" class="form-control"></form:input>
							  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</div>
						<div class="form-group">
							<label for="seatsInput">Seats: </label>
							<form:input path="seats" class="form-control" id="seats" name="seats"/>
						</div>
             	</div>
	             <div>
	                 <input type="submit" class="btn btn-primary" value="Add schedule"></input>
	            </div>
	            </fieldset>
            </form:form>
			

		</div>	<!-- main -->
      </div>	<!-- row -->
    </div>		<!-- container-fluid -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
    <script src="${jqueryUrl}"></script>
    <spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
    <script src="${bootstrapJsUrl}"></script>
 	<spring:url value="/js/bootstrap-datepicker.js" var="datePickerJs"></spring:url>
 	<script src="${datePickerJs}"></script>
    <spring:url value="/js/jquery.validate.js" var="jqueryValidator"></spring:url>
    <script type="text/javascript" src="${jqueryValidator}"></script> 	
    <spring:url value="/js/addSchedule.js" var="addScheduleJs"></spring:url>
 	<script type="text/javascript" src="${addScheduleJs}"></script>
  </body>
</html>
