<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>



	<table class="table table-striped rtable" id="fullScheduleTable">
		<tr>
			<th>Route</th>
			<th>Station from</th>
			<th>Station to</th>
			<th>Departure time</th>
			<th>Arrival time</th>
			<th></th>
		</tr>
		<c:forEach items = "${scheduleList}" var = "line">
			<tr>
				<td>${line.route.routeName}</td>
				<td>${line.route.routeLines.get(0).station.name}</td>
				<td>${line.route.stationTo.name}</td>
				<td><fmt:formatDate value="${line.stationFromDateTime}" pattern="dd.MM.yyyy HH:mm:ss"/></td>
				<td><fmt:formatDate value="${line.stationToDateTime}" pattern="dd.MM.yyyy HH:mm:ss"/></td>
				<spring:url value="/buyTicket/form?id=${line.id}" var="buyTicketUrl"/>
				<spring:url value="/passengerList?id=${line.id}" var="passengerListUrl"></spring:url>
				<spring:url value="/schedule/routeLines?id=${line.id}" var="scheduleRouteLinesUrl"></spring:url>
				<spring:url value="/img/buy-tickets-button2.png" var="buyTicketImg"></spring:url>
				<spring:url value="/img/list-button.png" var="stationListImg"></spring:url>
				<td>
					<sec:authorize access="isAuthenticated()">													
						<a href="${passengerListUrl}"><img alt="Passenger list" title="View passenger list" src="img/passenger-list-btn.png" height="30"></a>
					</sec:authorize>
					<a href="${scheduleRouteLinesUrl}"><img src="${stationListImg}" title="View station list" height="32"/></a>
					<c:if test="${line.haveTimeToBuy()}">
						<a href="${buyTicketUrl}"><img alt="Buy ticket" src="${buyTicketImg}" height="32"/></a>
					</c:if>					
				</td>
			</tr>
		</c:forEach>
	</table>
