<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<!DOCTYPE html>
<html>
  <head>
    <title>Schedule</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet">
	<spring:url value="css/datepicker3.css" var="datePickerCssUrl"></spring:url>
	<link href="${datePickerCssUrl}" rel="stylesheet">
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">
	<spring:url value="/css/rtable.css" var="rTableCss"></spring:url>
	<link href="${rTableCss}" rel="stylesheet">
	<spring:url value="/css/index.css" var="indexCssUrl"></spring:url>
	<link href="${indexCssUrl}" rel="stylesheet">
  </head>

  <body>
    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>
		
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<div class="rheader col-md-9">
        		<div class="rheader-buttons">
	        		<jsp:include page="scheduleFilterForm.jsp"></jsp:include>
	        	</div>
	        	<c:if test="${not empty msg}">
		        	<div class="alert alert-danger rheader-message">
		        		<a class="close" data-dismiss="alert">�</a>
						${msg}
					</div>
				</c:if>
        	</div>
        	
          <div class="table-responsive" id="tableContainer">            
			<jsp:include page="fullScheduleTable.jsp"></jsp:include>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
    <script src="${jqueryUrl}"></script>
    <spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
    <script src="${bootstrapJsUrl}"></script>
 	<spring:url value="/js/bootstrap-datepicker.js" var="datePickerJs"></spring:url>
 	<script src="${datePickerJs}"></script> 	
    <spring:url value="/js/index.js" var="indexJsUrl"></spring:url>
 	<script type="text/javascript" src="${indexJsUrl}"></script>
    
  </body>
</html>
