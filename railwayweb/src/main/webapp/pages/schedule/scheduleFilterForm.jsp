<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


			<div id = "filters">
				<form:form method = "post" modelAttribute="applyFilter" class="form-inline" role="form">
					
						<div class="form-group">
								<form:select path="stationIdFrom" class="form-control">
									<form:option value="0" label="--Station from--"></form:option>
									<form:options items="${stationMap}"/>
								</form:select>
						</div>
						<div class="form-group">
								<form:select path="stationIdTo" class="form-control">
									<form:option value="0" label="--Station to--"></form:option>
									<form:options items="${stationMap}"/>
								</form:select>
						</div>
						<div class="input-group date col-md-4 form-group" id="dpFilter">
							<div style="float:left">
							<form:input path="dateStart" type="text" class="form-control" style="width: 75%"></form:input>
							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>							
						</div>
					
						<div class="form-group">
							<input type="submit" class="btn btn-success" value="Apply filter"/>
						</div>
					
				</form:form>				
			</div>
			