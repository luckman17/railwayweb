<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<!DOCTYPE html>
<html>
  <head>
    <title>Schedule Lines</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet">
	<spring:url value="css/datepicker3.css" var="datePickerCssUrl"></spring:url>
	<link href="${datePickerCssUrl}" rel="stylesheet">
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">
	<spring:url value="/css/rtable.css" var="rTableCss"></spring:url>
	<link href="${rTableCss}" rel="stylesheet">
	
    <spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
    <script src="${jqueryUrl}"></script>
 	<spring:url value="/js/bootstrap-datepicker.js" var="datePickerJs"></spring:url>
 	<script src="${datePickerJs}"></script>
  </head>

  <body>
    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>
		
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<div class="rheader col-md-8">
        		<div class="rheader-buttons">
        			<strong>${infoMsg}</strong>
        		</div>        	
	        	<c:if test="${not empty msg}">
		        	<div class="alert alert-danger rheader-message">
		        		<a class="close" data-dismiss="alert">�</a>
						${msg}
					</div>
				</c:if>
        	</div>
        	
          <div class="table-responsive" id="tableContainer">
          
          	<table class="table rtable">
          		<tr>
          			<th> Station </th>
          			<th> Passage time </th>
          		</tr>
          		<c:forEach items="${scheduleRouteLines}" var="tableRow">
          			<tr>
	          			<td>${tableRow.stationName}</td>
	          			<td><fmt:formatDate value="${tableRow.passageDateTime}" pattern="dd.MM.yyyy HH:mm:ss"/></td>
          			</tr>
          		</c:forEach>
          		
          	</table>            
			
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
    <script src="${bootstrapJsUrl}"></script>
  </body>
</html>
