<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<head>
	<spring:url value="/css/sidebar.css" var="sidebarUrl"></spring:url>
	<link href="${sidebarUrl}" rel="stylesheet">
</head>
<body>
	<div class="col-sm-3 col-md-2 sidebar">
		<ul class="nav nav-sidebar">
			<spring:url value="/" var="scheduleUrl"></spring:url>
			<li id="scheduleMenu"><a href="${scheduleUrl}">Schedule</a></li>
		</ul>
		
	    <sec:authorize access="isAuthenticated()">
			<c:set var="displayName">
		        <sec:authentication property="principal.username" />
			</c:set>
			logged in as:${displayName}
			<br>
			<spring:url value="/logout" var="logoutUrl"/>
			<a href="${logoutUrl}">logout</a>			
	    </sec:authorize>
	    
	    <sec:authorize access="isAnonymous()">
	    	<jsp:include page="loginForm.jsp"></jsp:include>
	    </sec:authorize>
		
		<sec:authorize access="isAuthenticated()">		
			<ul class="nav nav-sidebar">
		       	<spring:url value="/station" var="stationUrl"></spring:url>
				<li id="stationListMenu"><a href="${stationUrl}">Station list</a></li>
				<spring:url value="/route" var="routeUrl"></spring:url>
				<li id="routeMenu"><a href="${routeUrl}">Routes</a></li>
				<spring:url value="/schedule/add" var="addScheduleUrl"></spring:url>
				<li id="addScheduleMenu"><a href="${addScheduleUrl}">Add schedule</a></li>
			</ul>
		</sec:authorize>
	</div>
</body>
			