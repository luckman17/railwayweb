<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
  <head>
    <title>Stations</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet"/>
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">
	<spring:url value="/css/rtable.css" var="rTableCss"></spring:url>
	<link href="${rTableCss}" rel="stylesheet">	
	<spring:url value="/css/station.css" var="stationStyleUrl"></spring:url>
	<link href="${stationStyleUrl}" rel="stylesheet"/>
  </head>

  <body>
    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
			<div class="rheader col-md-6">
				<div class="rheader-buttons">
					<a href="#stationModal" id="showModalBtn" role="button" class="btn btn-primary" data-toggle="modal">Add station</a>	
				</div>
	        	<form:form modelAttribute="station">
					<c:set var="formErrors"><form:errors path="*"/></c:set>
				    <c:if test="${not empty formErrors}">
						<div class="alert alert-danger rheader-message">  
							<a class="close" data-dismiss="alert">�</a>
	 						<form:errors path="name"></form:errors>	  
						</div>
				    </c:if>
	        	</form:form>
	        	<c:if test="${not empty msg}">
					<div class="alert alert-danger rheader-message">  
						<a class="close" data-dismiss="alert">�</a>
 						${msg}	  
					</div>
	        	</c:if>
        	</div>
			
			<div id="tableContainer">
				<jsp:include page="stationTable.jsp"></jsp:include>
			</div>
        
        </div>	<!-- main -->
        
		<div class="modal fade" id="stationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		             <div class="modal-header">
		                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                 <h4 class="modal-title" id="myModalLabel">Add station</h4>
		             </div>
		             <form:form method="post" modelAttribute="station">
		             	<div class="modal-body">
							<table class="form-group">
								<tr>
									<td><b>Station name: </b></td>
									<td> <form:input path="name" class="form-control"/> </td>
								</tr>
								<tr>
									<td/>
									<td><form:errors path="name"></form:errors></td>
								</tr>
							</table>		                  
		             	</div>
			             <div class="modal-footer">
			                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			                 <input type="submit" class="btn btn-primary" value="Add station"></input>
			            </div>
		            </form:form>
		        </div>
		     </div>
		</div>        
        
      </div>	<!-- row -->
    </div>		<!-- container-fluid -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
    <script src="${jqueryUrl}"></script>
    <spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
    <script src="${bootstrapJsUrl}"></script>
    <spring:url value="/js/stationjs.js" var="stationJs"></spring:url>
 	<script type="text/javascript" src="${stationJs}"></script>
  </body>
</html>
