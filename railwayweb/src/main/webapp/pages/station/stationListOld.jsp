<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<head>
	<link href="css/stationStyle.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="js/stationjs.js"></script>
</head>
<body>
	
	<div class="container">
		<a href="javascript:PopUpShow()">Show popup</a>
	</div>
	
	<div class="popup" id="addStationPopup">
		<div class="popupContent">
			<div id="stationForm">
				<form:form method="post" modelAttribute="stationForm">
					<table id="stationTable">
						<tr>
							<td> Station name: </td>
							<td> <form:input path="name"/> </td>
						</tr>
					</table>
					<input type="submit" value = "Add station"/>					
				</form:form>
			</div>
		</div>
	</div>
</body>