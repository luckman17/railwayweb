<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<table class="table rtable">
	<tr>
		<th class="col-md-2 text-center">Station id</th>
		<th>Station name</th>
		<th></th>
	</tr>
	<c:forEach items = "${stationList}" var = "line">
		<tr>
			<td class="text-center">${line.id}</td>
			<td>${line.name}</td>
			<td>
				<spring:url value="/station/delete?id=${line.id}" var="deleteStationUrl"></spring:url>
				<spring:url value="/img/delete-button.png" var="deleteButtonImg"></spring:url>
				<a href="${deleteStationUrl}"><img src="${deleteButtonImg}" height=30></img></a>
			</td>
		</tr>
	</c:forEach>
</table>
