<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <title>Ticket</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet"/>
	<spring:url value="/css/addSchedule.css" var="addScheduleUrl"></spring:url>
	<link href="${addScheduleUrl}" rel="stylesheet"/>
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">	
</head>
<body>
    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<c:if test="${empty msg}">
        		<div class="rform-centered col-md-8">
        			<h3> Ticket bought! </h3>
        			<br/><br/>
        			<h4> Information: </h4>
	        		<table class="table table-bordered">
	        			<tr>
	        				<td> First name: </td> <td> ${passengerTicket.firstname}</td>
	        			</tr>
	        			<tr>
	        				<td> Last name: </td> <td> ${passengerTicket.lastname}</td>
	        			</tr>
	        			<tr>
	        				<td> Station from: </td> <td> ${passengerTicket.stationFrom}</td>
	        			</tr>
	        			<tr>
	        				<td> Station to: </td> <td> ${passengerTicket.stationTo} </td>
	        			</tr>
	        		</table>
        		</div>
        	</c:if>
        	<c:if test="${not empty msg}">
        		<h3>${msg}</h3>
        	</c:if>

		</div>	<!-- main -->
      </div>	<!-- row -->
    </div>		<!-- container-fluid -->
	
<spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
<script src="${jqueryUrl}"></script>
<spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
<script src="${bootstrapJsUrl}"></script>
	
</body>