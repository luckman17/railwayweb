<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <title>Buy Ticket</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet"/>
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">
	<spring:url value="/css/buyTicketPassenger.css" var="buyTicketPassengerCss"></spring:url>
	<link href="${buyTicketPassengerCss}" rel="stylesheet">	
</head>
<body>

    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        	<div class="rheader col-md-8">
	        	<c:if test="${not empty msg}">
		        	<div class="alert alert-danger rheader-message">
		        		<a class="close" data-dismiss="alert">�</a>
						${msg}
					</div>
				</c:if>
        	</div>


			<form:form method="post" modelAttribute="buyTicketDto" action="/buyTicket/result" class="form col-md-4" id="buyTicketPassengerForm">
				<form:hidden path="scheduleId" value="${buyTicketDto.scheduleId}"/>
				<form:hidden path="pointFrom" value="${buyTicketDto.pointFrom}"/>
				<form:hidden path="pointTo" value="${buyTicketDto.pointTo}"/>
				<c:set var="formErrors"><form:errors path="*"></form:errors></c:set>
				<c:if test="${not empty formErrors}">
		        	<div class="alert alert-danger rheader-message">
		        		<a class="close" data-dismiss="alert">�</a>
						<form:errors path="*"></form:errors>
					</div>
				</c:if>
				<fieldset>								
					<div class="form-group">						
							<label for="passFirstname"> First Name: </label>
							<form:input path="passenger.firstname" class="form-control" id="passFirstname"/>								
					</div>
					<div class="form-group">
						<label for="passLastname"> Last Name: </label>
						<form:input path="passenger.lastname" class="form-control" id="passLastname"/>
					</div>
					<div class="form-group">
						<label for="passBirthdate"> Birthdate: </label>
						<form:input path="passenger.birthdate" class="form-control" id="passBirthdate"/>
					</div>					
				<input type="submit" class="btn btn-success" value="Buy ticket"/>
				</fieldset>
			</form:form>
	
		</div>	<!-- main -->
      </div>	<!-- row -->
    </div>		<!-- container-fluid -->
		
    <spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
    <script src="${jqueryUrl}"></script>
    <spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
    <script src="${bootstrapJsUrl}"></script>
    <spring:url value="/js/jquery.validate.js" var="jqueryValidator"></spring:url>
    <script type="text/javascript" src="${jqueryValidator}"></script>
    <spring:url value="/js/buyTicketPassenger.js" var="buyTicketPassengerJs"></spring:url>
    <script type="text/javascript" src="${buyTicketPassengerJs}"></script>

</body>