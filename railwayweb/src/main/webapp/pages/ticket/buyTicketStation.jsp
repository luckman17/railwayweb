<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <title>Buy Ticket</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet"/>
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">
	<spring:url value="/css/buyTicketStation.css" var="buyTicketStationCss"></spring:url>
	<link href="${buyTicketStationCss}" rel="stylesheet">
	
    <spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
    <script src="${jqueryUrl}"></script>
</head>
<body>
    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        	<div class="rheader col-md-8">
	        	<c:if test="${not empty msg}">
		        	<div class="alert alert-danger rheader-message">
		        		<a class="close" data-dismiss="alert">�</a>
						${msg}
					</div>
				</c:if>
        	</div>

				<form:form method="post" modelAttribute="buyTicketDto" class="form col-md-4" id="buyTicketStationForm">
			  		<form:hidden value="${param.id}" path="scheduleId"/>
						<div class="form-group">
							<label for="stationFromSelect"> Station from: </label>
								<form:select path="pointFrom" class="form-control" id="stationFromSelect">
									<c:forEach var="entry" items="${stationMap}">
										<form:option value="${entry.key}">${entry.value.name}</form:option>
									</c:forEach>
								</form:select>
						</div>
						<div class="form-group">
							<label for="stationToSelect"> Station to: </label>
								<form:select path="pointTo" class="form-control" id="stationToSelect">
									<c:forEach var="entry" items="${stationMap}">
										<form:option value="${entry.key}">${entry.value.name}</form:option>
									</c:forEach>
								</form:select>
						</div>
					<input class="btn btn-primary" type="submit" value="Next"/>	
				</form:form>
	
	
		</div>	<!-- main -->
      </div>	<!-- row -->
    </div>		<!-- container-fluid -->
	
</body>