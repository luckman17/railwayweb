<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
  <head>
    <title>Passenger list</title>
	<spring:url value="/css/bootstrap/bootstrap.min.css" var="bootstrapMinUrl"></spring:url>    
	<link href="${bootstrapMinUrl}" rel="stylesheet"/>
	<spring:url value="/css/rheader.css" var="rheaderUrl"></spring:url>
	<link href="${rheaderUrl}" rel="stylesheet">
	<spring:url value="/css/rtable.css" var="rTableCss"></spring:url>
	<link href="${rTableCss}" rel="stylesheet">
  </head>

  <body>
    <div class="container-fluid">
      <div class="row">
		<jsp:include page="../sidebar.jsp"></jsp:include>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<div class="rheader">
        		<div class="rheader-buttons">
        			<strong>Total passengers: ${passengerList.size()}</strong>
        		</div>
        	</div>
        	
        	<div id="tableContainer">
        	<table class="table rtable">
        		<tr>
        			<th>First name</th>
        			<th>Last name</th>
        			<th>Birthdate</th>
        			<th>Station from</th>
        			<th>Station to</th>        			
        		</tr>
        		<c:forEach items="${passengerList}" var="tableRow">
        			<tr>
        				<td>${tableRow.firstname}</td>
        				<td>${tableRow.lastname}</td>
        				<td>${tableRow.birthdate}</td>
        				<td>${tableRow.stationFrom}</td>
        				<td>${tableRow.stationTo}</td>
        			</tr>
        		</c:forEach>
        	</table>
        	</div>
        
		</div>	<!-- main -->        
      </div>	<!-- row -->
    </div>		<!-- container-fluid -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <spring:url value="/js/jquery-2.1.0.js" var="jqueryUrl"></spring:url>
    <script src="${jqueryUrl}"></script>
    <spring:url value="/js/bootstrap/bootstrap.min.js" var="bootstrapJsUrl"/>
    <script src="${bootstrapJsUrl}"></script>
  </body>
</html>
