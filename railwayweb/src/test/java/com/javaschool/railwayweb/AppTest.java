package com.javaschool.railwayweb;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.javaschool.railwayweb.dao.RouteDao;
import com.javaschool.railwayweb.dao.ScheduleDao;
import com.javaschool.railwayweb.model.Route;
import com.javaschool.railwayweb.model.Schedule;
import com.javaschool.railwayweb.model.Ticket;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testApp()
    {
    	ScheduleDao scheduleDao = new ScheduleDao();
    	RouteDao routeDao = new RouteDao();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("railwayticket");		
		EntityManager em = emf.createEntityManager();
		scheduleDao.setEntityManager(em);
		routeDao.setEntityManager(em);
    	
		try {
			Schedule s = scheduleDao.find(1);
			for (Ticket t : s.getTickets()) {
				System.out.println(t.getPassenger().getId());
			}
	        Route r = routeDao.find(1);
	        System.out.println("line size == " + r.getRouteLines().size());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        assertTrue(true);
    }
}
